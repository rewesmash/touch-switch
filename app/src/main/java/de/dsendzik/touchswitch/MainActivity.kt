package de.dsendzik.touchswitch

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.Settings
import android.widget.Button

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val touchOn = findViewById<Button>(R.id.on)
        val touchOff = findViewById<Button>(R.id.off)

        touchOn.setOnClickListener {
            Settings.System.putInt(contentResolver, "show_touches", 1)
        }

        touchOff.setOnClickListener {
            Settings.System.putInt(contentResolver, "show_touches", 0)
        }
    }
}
